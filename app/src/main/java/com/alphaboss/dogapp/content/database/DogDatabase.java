package com.alphaboss.dogapp.content.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.Log;

import com.alphaboss.dogapp.content.User;

public class DogDatabase extends SQLiteOpenHelper {
    public static final String TAG = DogDatabase.class.getSimpleName();

    public static final String TABLE_USERS = "users";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_USERNAME = "username";
    public static final String COLUMN_PASSWORD = "password";
    public static final String COLUMN_TOKEN = "token";

    private static final String DATABASE_NAME = "dog.db";
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_CREATE = "create table "
            + TABLE_USERS + "( " + COLUMN_ID
            + " integer primary key autoincrement, "
            + COLUMN_USERNAME + " text not null, "
            + COLUMN_PASSWORD + " text not null, "
            + COLUMN_TOKEN + " text not null);";

    public DogDatabase(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database) {
        database.execSQL(DATABASE_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_USERS);
        onCreate(db);
    }

    public void saveUser(User user) {
        ContentValues cv = new ContentValues();
        cv.put(COLUMN_USERNAME, user.getUsername());
        cv.put(COLUMN_PASSWORD, user.getPassword());
        cv.put(COLUMN_TOKEN, user.getToken());
        SQLiteDatabase db = getWritableDatabase();
        db.insert(TABLE_USERS, null, cv);
        db.close();
    }

    public User getCurrentUser() {
        String getUsernameSelect = "select " + COLUMN_USERNAME + "," + COLUMN_PASSWORD + "," + COLUMN_TOKEN + " from " + TABLE_USERS;
        Cursor c = getReadableDatabase().rawQuery(getUsernameSelect, null, null);
        if (c.moveToFirst()) {
            return new User(c.getString(0), c.getString(1), c.getString(2));
        } else {
            return null;
        }
    }

    public boolean deleteUser(User currentUser) {
        SQLiteDatabase db = getWritableDatabase();
        int deleted = db.delete(TABLE_USERS,
                String.format("%s = ? and %s = ?", COLUMN_USERNAME, COLUMN_PASSWORD),
                new String[] { currentUser.getUsername(), currentUser.getPassword() });
        db.close();
        if (deleted == 0) {
            return false;
        } else {
            return true;
        }
    }
}
