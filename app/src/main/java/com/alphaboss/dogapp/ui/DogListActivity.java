package com.alphaboss.dogapp.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.alphaboss.dogapp.DogApp;
import com.alphaboss.dogapp.R;
import com.alphaboss.dogapp.content.Dog;
import com.alphaboss.dogapp.util.Cancellable;
import com.alphaboss.dogapp.util.DialogUtils;
import com.alphaboss.dogapp.util.OnErrorListener;
import com.alphaboss.dogapp.util.OnSuccessListener;
import com.squareup.picasso.Picasso;

import java.util.List;

public class DogListActivity extends AppCompatActivity {
    public static final String TAG = DogListActivity.class.getSimpleName();
    private DogApp mApp;
    private Cancellable mGetDogsAsyncCall;
    private ProgressBar mContentLoadingView;
    private Button mRefreshBtn;
    private RecyclerView mRecyclerView;
    private int GRID_ITEMS = 2;
    public static final String LOGOUT = "logout";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate");
        super.onCreate(savedInstanceState);
        mApp = (DogApp) getApplication();
        setContentView(R.layout.activity_dog_list);
        setupToolbar();
        setupFloatingActionBar();
        setupContentLoading();
        setupRecyclerView();
        setupLogout();
    }

    private void setupLogout() {
        FloatingActionButton fabLogout = (FloatingActionButton) findViewById(R.id.fabLogout);
        fabLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean deleted = mApp.getDogManager().logout();
                if (deleted) {
                    Intent myIntent = new Intent(DogListActivity.this, LoginActivity.class);
                    myIntent.putExtra(LOGOUT, deleted); //Optional parameters
                    finish();// kill current activity
                    DogListActivity.this.startActivity(myIntent);
                } else {
                    Snackbar.make(v, "Logout failed", Snackbar.LENGTH_LONG).show();
                }
            }
        });
    }

    @Override
    protected void onStart() {
        Log.d(TAG, "onStart");
        super.onStart();
        startGetDogsAsyncCall();
        mApp.getDogManager().subscribeChangeListener();
    }

    @Override
    public void onStop() {
        Log.d(TAG, "onStop");
        super.onStop();
        ensureGetDogsAsyncCallCancelled();
        mApp.getDogManager().unsubscribeChangeListener();
    }

    private void setupToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle(getTitle());
    }

    private void setupFloatingActionBar() {
        FloatingActionButton fabCreate = (FloatingActionButton) findViewById(R.id.fabCreate);
        fabCreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(DogListActivity.this, DogCreateInputActivity.class);
                DogListActivity.this.startActivity(myIntent);
            }
        });
    }

    private void setupRecyclerView() {
        mRecyclerView = (RecyclerView) findViewById(R.id.dog_list);
    }

    private void setupContentLoading() {
        mContentLoadingView = (ProgressBar) findViewById(R.id.content_loading);
        mRefreshBtn = (Button) findViewById(R.id.refresh_btn);

        mRefreshBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startGetDogsAsyncCall();
            }
        });
    }

    private void startGetDogsAsyncCall() {
        showLoadingIndicator();
        new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() {
                        mGetDogsAsyncCall = mApp.getDogManager().getDogsAsync(
                                new OnSuccessListener<List<Dog>>() {
                                    @Override
                                    public void onSuccess(final List<Dog> dogs) {
                                        Log.d(TAG, "getDogsAsyncCall - success");
                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                showContent(dogs);
                                            }
                                        });
                                    }
                                }, new OnErrorListener() {
                                    @Override
                                    public void onError(final Exception e) {
                                        Log.d(TAG, "getDogsAsyncCall - error");
                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                showError(e);
                                            }
                                        });
                                    }
                                }
                        );
                    }
                }, 1000);

    }

    private void ensureGetDogsAsyncCallCancelled() {
        if (mGetDogsAsyncCall != null) {
            Log.d(TAG, "ensureGetDogsAsyncCallCancelled - cancelling the task");
            mGetDogsAsyncCall.cancel();
        }
    }

    private void showError(Exception e) {
        Log.e(TAG, "showError", e);
        if (mContentLoadingView.getVisibility() == View.VISIBLE) {
            disableLoadingIndicator();
        }
        DialogUtils.showError(this, e);
    }

    private void showContent(final List<Dog> dogs) {
        Log.d(TAG, "showContent");
        mRecyclerView.setAdapter(new DogRecyclerViewAdapter(dogs));
        disableLoadingIndicator();
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getApplicationContext(), GRID_ITEMS);
        mRecyclerView.setLayoutManager(layoutManager);
    }

    public class DogRecyclerViewAdapter extends RecyclerView.Adapter<DogRecyclerViewAdapter.ViewHolder> {

        private final List<Dog> mValues;

        public DogRecyclerViewAdapter(List<Dog> items) {
            mValues = items;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.dog_list_content, parent, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, int position) {
            holder.mItem = mValues.get(position);
            holder.mIdView.setText(mValues.get(position).getId());
            holder.mContentView.setText(mValues.get(position).getText());
            Picasso.with(getApplicationContext())
                    .load(mValues.get(position).getImg())
                    .resize(200, 200)
                    .centerCrop()
                    .placeholder(R.drawable.dog_placeholder)
                    .error(R.drawable.dog_placeholder_error)
                    .into(holder.mImgView);

            holder.mView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Context context = v.getContext();
                    Intent intent = new Intent(context, DogDetailActivity.class);
                    intent.putExtra(DogDetailFragment.DOG_ID, holder.mItem.getId());
                    intent.putExtra(DogDetailFragment.DOG_IMG, holder.mItem.getImg());
                    context.startActivity(intent);
                }
            });
        }

        @Override
        public int getItemCount() {
            return mValues.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            public final View mView;
            public final TextView mIdView;
            public final TextView mContentView;
            private final ImageView mImgView;
            public Dog mItem;

            public ViewHolder(View view) {
                super(view);
                mView = view;
                mIdView = (TextView) view.findViewById(R.id.id);
                mContentView = (TextView) view.findViewById(R.id.content);
                mImgView = (ImageView) view.findViewById(R.id.dog_img);
            }

            @Override
            public String toString() {
                return super.toString() + " '" + mContentView.getText() + "'";
            }
        }
    }

    public void disableLoadingIndicator() {
        mContentLoadingView.setVisibility(View.GONE);
        mRecyclerView.setVisibility(View.VISIBLE);
        mRefreshBtn.setVisibility(View.VISIBLE);
    }

    public void showLoadingIndicator() {
        Log.d(TAG, "showLoadingIndicator");
        mContentLoadingView.setVisibility(View.VISIBLE);
        mRecyclerView.setVisibility(View.GONE);
        mRefreshBtn.setVisibility(View.GONE);
    }
}
